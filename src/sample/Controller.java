package sample;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import java.io.File;

public class Controller {
    private static File startDirectory;

    @FXML
    private Label result;

    @FXML
    private TextField stringForSearch;

    @FXML
    private Button directorySelector;

    @FXML
    private Label directoryLine;

    @FXML
    private Button searchActivate;

    @FXML
    void initialize() {
        directorySelector.setOnAction(event -> {
            DirectoryChooser directory = new DirectoryChooser();
            directory.setTitle("Выберите стартовый каталог");
            startDirectory = new File(String.valueOf(directory.showDialog(new Stage())));
            directoryLine.setText(startDirectory.getPath());
            directory.setTitle(startDirectory.getPath());
        });

        searchActivate.setOnAction(event -> {
            new FileTextSearcher(String.valueOf(stringForSearch.getCharacters()), startDirectory.getPath()).run();
            result.setText("Файл ResultOfSearch был создан");

        });



    }
}
