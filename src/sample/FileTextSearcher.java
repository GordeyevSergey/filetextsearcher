package sample;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileTextSearcher extends Thread {
    private String text;
    private String directory;

    @Override
    public void run() {
        ArrayList<String> pathList = searchFiles(directory);
        assert pathList != null;
        for (String aPathList : pathList) {
            File file = new File(aPathList);
            fileTextSearcher(file, text);
        }
    }

    public FileTextSearcher(String text, String directory) {
        this.text = text;
        this.directory = directory;
    }

    private static ArrayList<String> searchFiles(String directory) {
        try (Stream<Path> paths = Files.walk(Paths.get(directory))) {
            List pathList = paths.filter(Files::isRegularFile).collect(Collectors.toList());
            ArrayList<String> filePathArray = new ArrayList<>();
            for (Object aPathList : pathList) {
                filePathArray.add(aPathList.toString());
            }
            return filePathArray;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void fileTextSearcher(File file, String text) {
        try (BufferedReader reader = new BufferedReader(new FileReader(file));
             BufferedWriter writer = new BufferedWriter(new FileWriter("ResultOfSearch", true))) {
            String string;
            int count = 0;
            while ((string = reader.readLine()) != null) {
                count++;
                if (string.contains(text)) {
                    writer.write("Текст найден в файле " + file.getName() + " в " + count + " строке " +
                            ", который находится " + file.getParent() + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}